package application;

import StageMaster.StageMaster;

import javafx.application.Application;
import javafx.stage.Stage;
import show_option.VMShowOption;

public class ApplicationMain extends Application{
		public static void main(String args[]) {
			db_operation.DbUtil.createDbConnection();
			launch(args);
			
		}
		public void start(Stage primaryStage) {
			StageMaster.setStage(primaryStage);
			new VMShowOption().show();
		}
		

	}


