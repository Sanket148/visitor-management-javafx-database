package show_option;

import check_in.CheckIn;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class ShowOptionController {

	@FXML
	private Button checkin;
	
	@FXML
	private Button checkout;
	
	
	
	public void checkin(ActionEvent event) {
		new CheckIn().show();
	}
	
}
